@extends('errors.template')

@section('title', 'Service Unavailable')
@section('details', 'FHM will be right back.')
