<?php
use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'API'], function () {
    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::get('lastfm/connect', 'LastfmController@connect');
        Route::post('lastfm/session-key', 'LastfmController@setSessionKey');
        Route::get('lastfm/callback', 'LastfmController@callback')->name('lastfm.callback');
        Route::delete('lastfm/disconnect', 'LastfmController@disconnect')->name('lastfm.disconnect');
    });
});
