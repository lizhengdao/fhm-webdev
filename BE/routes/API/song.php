<?php
use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'API'], function () {
    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::get('{song}/play/{transcode?}/{bitrate?}', 'SongController@play')->name('song.play');
        Route::post('{song}/scrobble/{timestamp}', 'ScrobbleController@store')->where([
            'timestamp' => '\d+',
        ]);
        Route::put('songs', 'SongController@update');
        Route::get('get_songs', 'SongController@getSongs');
        Route::post('upload', 'SongController@upload');
    });
});
