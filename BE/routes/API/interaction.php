<?php
use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'API'], function () {
    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::post('interaction/play', 'Interaction\PlayCountController@store');
        Route::post('interaction/like', 'Interaction\LikeController@store');
        Route::post('interaction/batch/like', 'Interaction\BatchLikeController@store');
        Route::post('interaction/batch/unlike', 'Interaction\BatchLikeController@destroy');
        Route::get('interaction/recently-played/{count?}', 'Interaction\RecentlyPlayedController@index')->where([
            'count' => '\d+',
        ]);
    });
});
