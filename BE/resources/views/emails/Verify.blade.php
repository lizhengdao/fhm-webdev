@component('mail::message')
#Hi {{$name}}!

We're excited to have you get started. First, you need to confirm your account. Just press the button below.

@component('mail::button', ['url' => $url])
Verify Account
@endcomponent

If you didn't make this request then you can safely ignore this email =]] 

@endcomponent
