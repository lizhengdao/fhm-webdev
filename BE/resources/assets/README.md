# fhm-core [![Build Status](https://travis-ci.org/fhm/core.svg?branch=master)](https://travis-ci.org/fhm/core) [![codecov](https://codecov.io/gh/fhm/core/branch/master/graph/badge.svg)](https://codecov.io/gh/fhm/core)

The core components and assets shared by the [web](https://github.com/phanan/fhm) and [desktop](https://github.com/phanan/fhm-app) versions of FHM.
